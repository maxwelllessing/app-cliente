package br.com.fluttersakaesapp;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;


public class MainActivity extends FlutterActivity {

    @Override
    protected void onStop() {
        super.onStop();
    }

    private MethodChannel.Result mResult;

    private MethodChannel methodChannel;

    private static final String CHANNEL = "sakaesapp.com/notification";

    public static final String mBroadcastNotificationAction = "br.com.fluttersakaesapp.client.notification";
    public static final String mBroadcastRestartAction = "br.com.fluttersakaesapp.client.restart";

    public static final String CHANNEL_ID = "br.com.fluttersakaesapp.client.timerappchannel";

    private IntentFilter mIntentFilter;

    private Intent mServiceIntent;
    private TimerAppService mTimerAppService;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals(mBroadcastRestartAction)) {
                Log.i(TimerAppService.class.getSimpleName(), mBroadcastRestartAction);
                startService(new Intent(context, TimerAppService.class));
            }
            if (intent.getAction() != null && intent.getAction().equals(mBroadcastNotificationAction)) {
                Log.i(TimerAppService.class.getSimpleName(), mBroadcastNotificationAction);
                methodChannel.invokeMethod("notification", "notification");
            }
        }
    };

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        //methodChannel = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL);

        //mTimerAppService = new TimerAppService();
        //mServiceIntent = new Intent(this, mTimerAppService.getClass());

        //mIntentFilter = new IntentFilter();
        //mIntentFilter.addAction(mBroadcastNotificationAction);
        //mIntentFilter.addAction(mBroadcastRestartAction);

        //if (!isMyServiceRunning(mTimerAppService.getClass())) {
        //  Log.i(TimerAppService.class.getSimpleName(), "start service");
        //   startService(mServiceIntent);
        //  Log.i(TimerAppService.class.getSimpleName(), "register receiver");
        //  registerReceiver(mReceiver, mIntentFilter);
        //}
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        //unregisterReceiver(mReceiver);
        super.onPause();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    @Override
    protected void onDestroy() {
        //stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }
}
