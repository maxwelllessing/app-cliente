package br.com.fluttersakaesapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

public class TimerAppService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.i(TimerAppService.class.getSimpleName(), "onStartCommand");

        startTimer(getApplicationContext());

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TimerAppService.class.getSimpleName(), "onDestroy");

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.mBroadcastNotificationAction);
        sendBroadcast(broadcastIntent);

        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer(Context context) {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(context);

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask(Context context) {
        timerTask = new TimerTask() {
            public void run() {

                Log.i(TimerAppService.class.getSimpleName(), "Call");

                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(MainActivity.mBroadcastNotificationAction);
                sendBroadcast(broadcastIntent);
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
