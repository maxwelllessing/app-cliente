import 'dart:ui';

class Constant {
  static const String URL = "http://192.168.10.137/apprests/public/";
  static const Color colorAccentColor = Color.fromRGBO(207, 61, 64, 1);
  static const String API_TOKEN = "API_TOKEN";
  static const String USER_ID = "USER_ID";
}