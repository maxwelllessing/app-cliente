import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/item.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import 'dart:math' as math;
import '../util.dart';

import '../screens/home_screen.dart';
import '../screens/product_screen.dart';

class DetailsScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsScreen> {
  List<Item> items = [];

  Order order;

  final txtCommentsController = TextEditingController();

  bool isLoadingButton = false;

  @override
  void initState() {
    super.initState();
  }

  bool isHistorical = false;

   ProductScreenNotificationOrder _notificationOrder;

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    if (arguments != null) {
      order = arguments['order'];
      isHistorical = arguments['isHistorical'];
      _notificationOrder = arguments['notificationOrder'];
    }

    setupList();

    return Scaffold(
      appBar: AppBar(
        title: Text("Detalhes do pedido"),
      ),
      body: Container(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(child: _buildStatusDescription()),
            SliverToBoxAdapter(child: _buildDividirHistorical()),
            SliverToBoxAdapter(child: _buildOrderNumber()),
            SliverToBoxAdapter(child: _buildDividirHistorical()),
            _buildList(items),
            SliverToBoxAdapter(child: _buildAddMoreItems()),
            SliverList(
              delegate: SliverChildListDelegate(
                [Divider(), _buildTotal()],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: !isHistorical ? _buildButton() : null,
    );
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            if (isLoadingButton) return;

            order.comments = txtCommentsController.text;

            //if (order.status == 1) order.status = 2;
            //if (order.status == 0) order.status = 1;

            setState(() {
              order = order;
            });

            isLoadingButton = true;

            Order.send(order).then((value) => {
                  isLoadingButton = false,
                  if (value != null)
                    {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => HomeScreen()),
                          (Route<dynamic> route) => false)
                    }
                });
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  isLoadingButton
                      ? CircularProgressIndicator(backgroundColor: Colors.white)
                      : Text("Fazer pedido", style: TextStyle(fontSize: 18))
                ],
              )),
          elevation: 5,
        ));
  }

  Widget _buildTotal() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: Column(children: <Widget>[
          Row(children: <Widget>[
            Text("Subtotal",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text(Util.numberFormatText(order.value),
                style: TextStyle(fontSize: 18))
          ]),
          Row(children: <Widget>[
            Text("Taxa de entrega",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text(Util.numberFormatText(0), style: TextStyle(fontSize: 18))
          ]),
          Row(children: <Widget>[
            Text("Valor total",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text(Util.numberFormatText(order.value),
                style: TextStyle(fontSize: 18))
          ])
        ]));
  }

  Widget _buildStatusDescription() {
    return isHistorical
        ? Padding(
            padding: EdgeInsets.all(16),
            child: Center(
                child: Text(order.statusDescription(),
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold))))
        : Container();
  }

  Widget _buildDividirHistorical() {
    return isHistorical ? Divider() : Container();
  }

  Widget _buildOrderNumber() {
    return isHistorical
        ? Padding(
            padding: EdgeInsets.all(16),
            child: Row(children: <Widget>[
              Text("Pedido nº" + order.id.toString(),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Spacer(),
              Text(Util.dateFormater(order.createdAt),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))
            ]))
        : Container();
  }

  Widget _buildAddMoreItems() {
    return !isHistorical
        ? Column(children: <Widget>[
            Divider(),
            InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Center(
                    child: Text("Adicionar mais items",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold))))
          ])
        : Container();
  }

  Widget _buildList(List<Item> list) {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        final int itemIndex = index ~/ 2;
        Item item = list[itemIndex];
        if (index.isEven) {
          return InkWell(
              child: Padding(
                  padding: EdgeInsets.all(8),
                  child: _buildRow(itemIndex, item)));
        }
        return Divider();
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, list.length * 2 - 1),
    ));
  }

  Row _buildRow(int index, Item item) {
    return Row(children: [
      Expanded(
          flex: 6, // 20%
          child: Text(item.amount.toString() + "x " + item.name.toString(),
              textAlign: TextAlign.start, style: TextStyle(fontSize: 18))),
      Expanded(
          flex: 3, // 20%
          child: Text(
              NumberFormat.currency(locale: "pt-BR", symbol: "R\$")
                  .format(item.value),
              style: TextStyle(fontSize: 18))),
      !isHistorical
          ? Expanded(
              flex: 1, // 20%
              child: IconButton(
                icon: new Icon(Icons.delete),
                onPressed: () => {__removeItem(index, item)},
              ))
          : Container()
    ]);
  }

  __removeItem(int index, Item item) {
    order.value -= item.value;
    order.items.removeAt(index);
    setupList();
    if (order.items.length <= 0) {
      Navigator.of(context).pop();
    }
  }

  void setupList() async {
    setState(() {
      items = order.items;
    });
    _notificationOrder(order);
  }
}
