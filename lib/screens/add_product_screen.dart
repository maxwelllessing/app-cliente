import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/models/variance.dart';
import '../constant.dart';
import '../models/product.dart';
import '../models/product.dart';
import '../models/product.dart';
import '../util.dart';
import 'row_varicance_checkbox_screen.dart';

class AddProuctScreenDialog extends StatefulWidget {
  final Product product;

  final List<Product> products;

  final List<int> selectedVariances;

  const AddProuctScreenDialog(
      {Key key, this.product, this.products, this.selectedVariances})
      : super(key: key);

  @override
  AddProuctScreenDialogState createState() =>
      new AddProuctScreenDialogState(product, products, selectedVariances);
}

class AddProuctScreenDialogState extends State<AddProuctScreenDialog> {
  Product product;

  AddProuctScreenDialogState(
      this.product, this.products, this.selectedVariances);

  List<int> selectedVariances = [];

  List<Product> products = [];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: _buildButton(),
      appBar: new AppBar(
        title: Text("Detalhes do item"),
        actions: [],
      ),
      body: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        var url = Constant.URL + "image?image=" + product.image;

        List<Widget> widgets = <Widget>[
          SliverToBoxAdapter(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(child: Image.network(url)),
              Divider(),
              Padding(
                  padding: EdgeInsets.only(right: 8, left: 8),
                  child: Text(product.name,
                      style: TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold))),
              Divider(),
              Padding(
                  padding: EdgeInsets.only(right: 8, left: 8),
                  child: Text(product.description,
                      style: TextStyle(fontSize: 12))),
              Divider()
            ],
          )),
        ];

        widgets.addAll(_buildListVariance(products));

        return CustomScrollView(slivers: widgets);
      }),
    );
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(8),
        child: Row(children: <Widget>[
          Visibility(
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              visible: product.amount != 0,
              child: IconButton(
                  icon: new Icon(Icons.remove),
                  onPressed: () => setState(() => product.amount > 1
                      ? product.amount--
                      : product.amount = 1))),
          Text(product.amount.toString()),
          new IconButton(
              icon: new Icon(Icons.add),
              onPressed: () => setState(() => product.amount++)),
          Spacer(),
          RaisedButton(
              child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("ADICIONAR "),
                        Text(Util.numberFormatText(valueOfProduct()))
                      ])),
              onPressed: hasSelectedVariances()
                  ? () => {
                        Navigator.of(context).pop({
                          "product": product,
                          "selectedVariances": selectedVariances,
                          "value": valueOfProduct()
                        })
                      }
                  : null)
        ]));
  }

  bool hasSelectedVariances() {
    if (product.type == 0) return true;
    if (products.length ==
        selectedVariances.where((element) => element != null).toList().length)
      return true;
    return false;
  }

  double valueOfProduct() {
    if (product.type == 1) {
      if (hasSelectedVariances())
        return products[0].variations[selectedVariances[0]].value;
      else
        return 0;
    }
    return product.value;
  }

  List<Widget> _buildListVariance(List<Product> list) {
    List<Widget> widgets = [];

    for (int i = 0; i < list.length; i++) {
      if (list[i].variations != null && list[i].variations.length > 0) {
        widgets.add(SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Text("Selecione sua opção (" + list[i].name + ")",
                    style: TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold)))));
        widgets.add(_buildList(list[i].variations, i));
        widgets.add(SliverToBoxAdapter(child: Divider()));
      }
    }

    return widgets;
  }

  Widget _buildList(List<Variance> list, int idProduct) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
      return InkWell(
          onTap: () => {},
          child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: RowVarianceCheckboxScreen(
                  list[index],
                  product,
                  index,
                  idProduct,
                  selectedVariances[idProduct],
                  (int idProduct, int value) => {
                        setState(() {
                          selectedVariances[idProduct] = value;
                        })
                      })));
    }, childCount: list.length));
  }
}
