import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/component/measure_size.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:fluttersakaesapp/models/category.dart';
import 'package:fluttersakaesapp/models/variance.dart';
import '../constant.dart';
import '../models/item.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import '../models/product.dart';
import '../screens/add_product_screen.dart';
import '../util.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;

import 'home_screen.dart';

class ProductScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  const ProductScreen({Key key}) : super(key: key);

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<ProductScreen>
    with SingleTickerProviderStateMixin {
  List<Category> list = [];

  List<Product> products = [];

  Order order;

  bool isLoading = true;

  int _stateButton = 0;

  TabController controller;

  TabBar bottomTabBar;

  int tabControllerLength = 0;

  ScrollController _scrollHeightController;

  ScrollController _scrollWidthController;

  List<Tab> tabs = [];

  AnimationController _controller;
  Animation<Offset> _offsetAnimationUp;
  Animation<Offset> _offsetAnimationDown;

  @override
  void initState() {
    super.initState();

    setupList();

    _scrollHeightController = ScrollController();
    _scrollWidthController = ScrollController();

    order = new Order(
        items: [],
        value: 0,
        status: 0,
        uiid: Uuid().v4(),
        createdAt: DateTime.now(),
        type: 1);

    SharedPreferencesHelper.instance().then((value) => {
          setState(() {
            order.userId = int.parse(value.getUserId());
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (Route<dynamic> route) => false);

        //we need to return a future
        return Future.value(false);
      },
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : Scaffold(
              appBar: AppBar(
                  title: Text("Sakae´s"),
                  bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(40.0),
                    child: Theme(
                      data:
                          Theme.of(context).copyWith(accentColor: Colors.white),
                      child: Container(
                        height: 40.0,
                        alignment: Alignment.center,
                        child: _buildScrollView3(),
                      ),
                    ),
                  )),
              body: _buildScrollView2(),
              bottomNavigationBar: order != null && order.items.length > 0
                  ? _buildButton()
                  : null),
    );
  }

  Widget setUpButtonChild() {
    if (_stateButton == 0) {
      return Text(
        "Detalhes (" + order.items.length.toString() + ")",
        style: TextStyle(fontSize: 18),
      );
    } else if (_stateButton == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return Icon(Icons.check, color: Colors.white);
    }
  }

  Widget _buildScrollView3() {
    return NotificationListener(
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: list.length,
          itemBuilder: (context, index) => MeasureSize(
              onChange: (size) {
                //list[index].widthItem = size.width;
              },
              child: InkWell(
                  onTap: () {
                    _setCategoria(index);
                  },
                  child: Column(children: <Widget>[
                    MeasureSize(
                        child: Padding(
                            padding: EdgeInsets.only(left: 8, right: 8),
                            child: Text(list[index].name,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18))),
                        onChange: (size) {
                          setState(() {
                            list[index].widthItem = size.width;
                          });
                        }),
                    Visibility(
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        visible: list[index].selected,
                        child: Padding(
                            padding: EdgeInsets.only(right: 4, top: 12),
                            child: Container(
                                height: 2.0,
                                width: list[index].widthItem,
                                color: Constant.colorAccentColor)))
                  ]))),
          controller: _scrollWidthController,
        ),
        onNotification: (t) {
          if (t is ScrollEndNotification) {}
          return true;
        });
  }

  Widget _buildScrollView2() {
    return NotificationListener(
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) => MeasureSize(
              onChange: (size) {
                setState(() {
                  list[index].heightItem = size.height;
                });
              },
              child: _buildRowCategory(list[index])),
          controller: _scrollHeightController,
          //itemScrollController: itemScrollController,
          //itemPositionsListener: itemPositionsListener,
        ),
        onNotification: (t) {
          if (t is ScrollEndNotification) {
            _findCategoria(_scrollHeightController.position.pixels);
          }
          return true;
        });
  }

  _findCategoria(double pixels) {
    double size = 0;

    for (int i = 0; i < list.length; i++) {
      list[i].selected = false;
    }

    for (int i = 0; i < list.length; i++) {
      size += list[i].heightItem;
      if (pixels <= size) {
        if (!list[i].selected) {
          if (i > 0) {
            _scrollWidthController.animateTo(list[i - 1].widthItem,
                duration: Duration(milliseconds: 500), curve: Curves.linear);
          } else {
            _scrollWidthController.animateTo(0,
                duration: Duration(milliseconds: 500), curve: Curves.linear);
          }
        }
        setState(() {
          list[i].selected = true;
        });
        break;
      }
    }
  }

  _setCategoria(int index) {
    double size = 0;

    for (int i = 0; i < list.length; i++) {
      list[i].selected = false;
    }

    if (index > 0) {
      for (int i = 0; i < list.length; i++) {
        size += list[i].heightItem;
        if (i == (index - 1)) {
          _scrollHeightController.animateTo(size + 1,
              duration: Duration(milliseconds: 500), curve: Curves.linear);
          setState(() {
            list[index].selected = true;
          });
          break;
        }
      }
    } else {
      _scrollHeightController.animateTo(0,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }
  }

  List<Widget> _buildScreen() {
    List<Widget> widgets = [];

    for (int i = 0; i < list.length; i++) {
      widgets.add(SliverToBoxAdapter(child: _buildRowCategory(list[i])));
      widgets.add(_buildListProduct(list[i].products));
    }

    return widgets;
  }

  Padding _buildRowCategory(Category item) {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 8, right: 8),
        child: Row(children: [
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                Text(item.name.toString(),
                    textAlign: TextAlign.start,
                    softWrap: true,
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                Divider(),
                Column(children: _buildProductList2(item.products))
                //_buildScrollView()
                //SliverToBoxAdapter(child: _buildListCategory(list))
              ]))
        ]));
  }

  List<Widget> _buildProductList2(List<Product> products) {
    List<Widget> w = [];
    for (int i = 0; i < products.length; i++) {
      w.add(_buildRowProduct(products[i]));
      w.add(Divider());
    }
    return w;
  }

  Widget _buildListProduct(List<Product> products) {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        final int itemIndex = index ~/ 2;
        Product product = products[itemIndex];
        if (index.isEven) {
          return InkWell(
              onTap: () => {_displayDialogProduct(product)},
              child: Padding(
                  padding:
                      EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
                  child: _buildRowProduct(product)));
        }
        return Divider();
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, products.length * 2 - 1),
    ));
  }

  _buildRowProduct(Product product) {
    int descriptionLength = product.description.length;

    String descriptionConcat = "";

    if (descriptionLength > 65) {
      descriptionLength = 65;
      descriptionConcat = "...";
    }

    return InkWell(
        onTap: () => {_displayDialogProduct(product)},
        child: Padding(
            padding: EdgeInsets.only(right: 8, left: 8),
            child: Row(children: [
              Expanded(
                  flex: 2, // 20%
                  child: Padding(
                      padding: EdgeInsets.only(right: 4),
                      child: Image.network(
                        Constant.URL + "image?image=" + product.image,
                        width: 75,
                        cacheWidth: 75,
                      ))),
              Expanded(
                flex: 6, // 20%
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(product.name.toString(),
                          textAlign: TextAlign.start,
                          softWrap: true,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      Text(
                          product.description
                                  .toString()
                                  .substring(0, descriptionLength) +
                              descriptionConcat,
                          maxLines: 3,
                          style: TextStyle(fontSize: 12)),
                    ]),
              ),
              Expanded(
                  flex: 2, // 20%
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                            NumberFormat.currency(
                                    locale: "pt-BR", symbol: "R\$")
                                .format(product.value),
                            style: TextStyle(fontSize: 14)),
                      ]))
            ])));
  }

  _addItem(Product product, List<int> selectedVariances, double value) {
    if (product.type == 0) {
      Item item = Item(
          name: product.name,
          value: value,
          amount: product.amount.toDouble(),
          uiid: Uuid().v4(),
          productId: product.id,
          comments: product.description,
          status: 0,
          orderId: order.id,
          printerId: product.printerId,
          ip: product.ip);

      setState(() {
        order.items.add(item);
        order.value += item.value;
        order.status = 0;
      });
    } else if (selectedVariances.length == 1) {
      Item item = Item(
          name: product.name,
          value: value,
          amount: product.amount.toDouble(),
          variances: product.variations[selectedVariances[0]].name,
          uiid: Uuid().v4(),
          productId: product.id,
          varianceId: product.variations[selectedVariances[0]].id,
          comments: product.description,
          status: 0,
          orderId: order.id,
          printerId: product.printerId,
          ip: product.ip);
      setState(() {
        order.items.add(item);
        order.value += item.value;
        order.status = 0;
      });
    } else {
      Item itemCombo = Item(
          name: product.name,
          value: value,
          amount: product.amount.toDouble(),
          uiid: Uuid().v4(),
          productId: product.id,
          comments: product.description,
          status: 0,
          orderId: order.id,
          printerId: product.printerId,
          ip: product.ip,
          items: []);
      for (int i = 0; i < selectedVariances.length; i++) {
        Product productItem = product.products[i];
        Item itemProduct = Item(
            name: productItem.name,
            value: value,
            amount: productItem.amount.toDouble(),
            variances: productItem.variations[selectedVariances[i]].name,
            uiid: Uuid().v4(),
            productId: productItem.id,
            varianceId: productItem.variations[selectedVariances[i]].id,
            comments: productItem.description,
            status: 0,
            printerId: productItem.printerId,
            ip: productItem.ip);
        itemCombo.items.add(itemProduct);
      }
      setState(() {
        order.items.add(itemCombo);
        order.value += itemCombo.value;
        order.status = 0;
      });
    }
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, 'details', arguments: {
              'order': order,
              'isHistorical': false,
              "notificationOrder": (order) => {_notificationOrder(order)}
            });
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[setUpButtonChild()],
              )),
          elevation: 5,
        ));
  }

  _notificationOrder(Order order) {
    setState(() {
      this.order = order;
    });
  }

  Future _displayDialogProduct(Product product) async {
    Map<String, dynamic> save = await Navigator.of(context)
        .push(new MaterialPageRoute<Map<String, dynamic>>(
            builder: (BuildContext context) {
              product.amount = 1;

              List<int> selectedVariances = [];

              List<Product> products =
                  product.products.length > 0 ? product.products : [product];

              for (int i = 0; i < products.length; i++) {
                selectedVariances.insert(i, null);
              }

              return new AddProuctScreenDialog(
                  product: product,
                  products: product.products.length > 0
                      ? product.products
                      : [product],
                  selectedVariances: selectedVariances);
            },
            fullscreenDialog: true));
    if (save != null) {
      _addItem(save["product"], save["selectedVariances"], save["value"]);
    }
  }

  void setupList() async {
    isLoading = true;
    Category.fetch().then((value) => setState(() {
          list = value;
          isLoading = false;
          list[0].selected = true;
        }));
  }
}

typedef ProductScreenNotificationOrder = void Function(Order order);
