import 'dart:async';
import 'dart:ffi';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';

import '../models/user.dart';

class SplashScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    new Timer(Duration(seconds: 3), () => {
      SharedPreferencesHelper.instance().then((value) => {
        if (value.getToken() == null)
          {Navigator.pushReplacementNamed(context, 'login')}
        else
          {
            User.fetchAccess(value.getToken()).then((value) => {
              if (value)
                {Navigator.pushReplacementNamed(context, '/')}
              else
                {
                  Navigator.pushReplacementNamed(
                      context, 'login')
                }
            })
          }
      })
    });

    return Center(child: Image(image: AssetImage('assets/splash.jpg')));
  }
}
