import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:fluttersakaesapp/models/notification_message.dart';
import 'package:fluttersakaesapp/screens/product_screen.dart';
import 'package:fluttersakaesapp/screens/order_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;
import '../component/print_socket.dart';
import 'package:rxdart/subjects.dart';
import '../main.dart';

class HomeScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  void _configureSelectNotificationSubject() {
    selectNotificationSubject.stream.listen((String payload) async {
      if (payload == "orders") {
        setState(() {
          _selectedIndex = 1;
        });
      }
    });
  }

  //notification() {
  //NotificationMessage.fetchNotification()
  //  .then((value) => {_showNotification(value)});
  // }

  List<Order> orders = [];

  bool isLoading = true;

  List<DropdownMenuItem<String>> _dropDownMenuItems;

  String _amountPeople;

  String _table;

  Timer timer;

  @override
  void initState() {
    super.initState();

    initFlutterLocalNotificationsPlugin();

    //_requestIOSPermissions();
    _configureSelectNotificationSubject();

    setupList();

    _dropDownMenuItems = getDropDownMenuItems();
    _table = _dropDownMenuItems[0].value;
    _amountPeople = _dropDownMenuItems[0].value;

    timer =
        new Timer.periodic(Duration(seconds: 30), (Timer t) => {setupList()});
  }

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    ProductScreen(),
    OrderScreen(),
    Text(
      'Index 4: School',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    if (arguments != null) {
      // setState(() {
      //_selectedIndex = arguments['tabSelected'];
      // });
    }

    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Inicio'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            title: Text('Pedidos'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Perfil'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Constant.colorAccentColor,
        onTap: (_onItemTapped),
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            _displayDialog(context);
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Text("Novo pedido", style: TextStyle(fontSize: 18))
                ],
              )),
          elevation: 5,
        ));
  }

  Widget _buildList(List<Order> list) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(),
      padding: EdgeInsets.all(10.0),
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
            child: Padding(
                padding: EdgeInsets.all(8), child: _buildRow(list[index])),
            onTap: () => {
                  Navigator.pushNamed(context, 'product',
                      arguments: {'order': list[index]})
                });
      },
    );
  }

  Row _buildRow(Order order) {
    return Row(children: [
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Text("Mesa nº" + order.table.toString(),
            textAlign: TextAlign.start,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Text("Nº" + order.table.toString() + " pessoas",
            style: TextStyle(fontSize: 18))
      ]),
      Spacer(),
      Column(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
        Text(DateFormat("dd/MM/yyyy HH:mm:ss").format(order.createdAt),
            style: TextStyle(fontSize: 18)),
        Text(
            NumberFormat.currency(locale: "pt-BR", symbol: "R\$")
                .format(order.value),
            style: TextStyle(fontSize: 18)),
      ])
    ]);
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 1; i < 99; i++) {
      items.add(new DropdownMenuItem(
          value: i.toString(), child: new Text(i.toString())));
    }
    return items;
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(child: Text('Novo pedido')),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Selecione a mesa",
                    textAlign: TextAlign.left,
                  ),
                  DropdownButton<String>(
                      value: _table,
                      items: _dropDownMenuItems,
                      onChanged: (String value) {
                        setState(() {
                          _table = value;
                        });
                      },
                      isExpanded: true),
                  Text("Selecione o nº de pessoas", textAlign: TextAlign.left),
                  DropdownButton<String>(
                      value: _amountPeople,
                      items: _dropDownMenuItems,
                      onChanged: (String value) {
                        setState(() {
                          _amountPeople = value;
                        });
                      },
                      isExpanded: true)
                ],
              );
            }),
            actions: <Widget>[
              new RaisedButton(
                child: new Text('CANCELAR'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _amountPeople = null;
                  _table = null;
                },
              ),
              new RaisedButton(
                child: new Text('CONFIRMAR'),
                onPressed: () {
                  if (_amountPeople != null && _table != null) {
                    Order order = new Order(
                        value: 0,
                        //table: int.parse(_table),
                        //amountPeople: int.parse(_amountPeople),
                        status: 0,
                        uiid: Uuid().v4(),
                        createdAt: DateTime.now(),
                        items: []);
                    Navigator.pushNamed(context, 'product',
                        arguments: {'order': order});
                  }
                },
              )
            ],
          );
        });
  }

  void setupList() async {
    isLoading = true;

    Order.fetch().then((value) => {
          if (mounted)
            {
              setState(() {
                orders = value;
                isLoading = false;
              })
            }
        });
  }

  void sendPrint() {}
}
