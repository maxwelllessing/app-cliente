import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import '../models/user.dart';
import 'package:google_sign_in/google_sign_in.dart';
import "package:http/http.dart" as http;
import 'dart:convert' show json;
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>['email'],
);

class LoginScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> {
  final formKey = new GlobalKey<FormState>();

  String _password, _email;

  static const double margin = 32;

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  Future<void> _handleSignOut() => _googleSignIn.disconnect();

  Future<Null> _login() async {
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;

        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${accessToken.token}');
        final profile = json.decode(graphResponse.body);

        User.fetchCreate(User(
                facebookId: profile["id"],
                name: profile["name"],
                email: profile["email"],
                type: 2))
            .then((user) => {
                  if (user != null)
                    {
                      SharedPreferencesHelper.instance().then((value) => {
                            value.setToken(user.apiToken),
                            value.setUserId(user.id.toString()),
                            Navigator.pushReplacementNamed(context, '/')
                          })
                    }
                });
        break;
      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      if (account != null) {
        User.fetchCreate(User(
                googleId: account.id,
                name: account.displayName,
                email: account.email,
                type: 2))
            .then((user) => {
                  if (user != null)
                    {
                      SharedPreferencesHelper.instance().then((value) => {
                            value.setToken(user.apiToken),
                            value.setUserId(user.id.toString()),
                            Navigator.pushReplacementNamed(context, '/')
                          })
                    }
                });
      }
    });
    //_googleSignIn.signInSilently();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(
                    left: margin, right: margin, bottom: margin),
                child: Text('Sakae\'s Restaurante',
                    style: TextStyle(fontSize: 24))),
            new Form(
                key: formKey,
                child: new Column(children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.only(left: margin, right: margin),
                    child: new TextFormField(
                      onSaved: (val) => _email = val,
                      validator: (val) {
                        return val.length < 4 ? "Digite seu email" : null;
                      },
                      decoration: new InputDecoration(labelText: "Email"),
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(left: margin, right: margin),
                    child: new TextFormField(
                        onSaved: (val) => _password = val,
                        decoration: new InputDecoration(labelText: "Senha"),
                        style: TextStyle(fontSize: 18),
                        obscureText: true),
                  ),
                  Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(16),
                        child: RaisedButton(
                          onPressed: () {
                            final form = formKey.currentState;

                            if (form.validate()) {
                              form.save();

                              User.fetchLogin(User(
                                      email: this._email,
                                      password: this._password))
                                  .then((user) => {
                                        if (user != null)
                                          {
                                            SharedPreferencesHelper.instance()
                                                .then((value) => {
                                                      value.setToken(
                                                          user.apiToken),
                                                      value.setUserId(
                                                          user.id.toString()),
                                                      Navigator
                                                          .pushReplacementNamed(
                                                              context, '/')
                                                    })
                                          }
                                      });
                            }
                          },
                          child: Padding(
                              padding: EdgeInsets.all(16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                // Replace with a Row for horizontal icon + text
                                children: <Widget>[
                                  Text("Login", style: TextStyle(fontSize: 18))
                                ],
                              )),
                          elevation: 5,
                        )),
                    SignInButton(
                      Buttons.Google,
                      text: "Login com Google",
                      onPressed: () {
                        _handleSignIn();
                      },
                    ),
                    SignInButton(
                      Buttons.Facebook,
                      text: "Login com Facebook",
                      onPressed: () {
                        _login();
                      },
                    ),
                  ]),
                ]))
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
