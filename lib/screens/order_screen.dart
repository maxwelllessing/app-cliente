import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:fluttersakaesapp/screens/product_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;
import '../component/print_socket.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  List<Order> ordersPrevious = [];

  List<Order> ordersProgress = [];

  bool isLoading = true;

  List<DropdownMenuItem<String>> _dropDownMenuItems;

  String _amountPeople;

  String _table;

  Timer timer;

  @override
  void initState() {
    super.initState();

    setupList();

    _dropDownMenuItems = getDropDownMenuItems();
    _table = _dropDownMenuItems[0].value;
    _amountPeople = _dropDownMenuItems[0].value;

    timer =
        new Timer.periodic(Duration(seconds: 30), (Timer t) => {setupList()});
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(40.0),
                    child: Theme(
                        data: Theme.of(context)
                            .copyWith(accentColor: Colors.white),
                        child: TabBar(
                          tabs: [
                            Tab(
                                child: Text("Em andamento",
                                    style: TextStyle(fontSize: 18))),
                            Tab(
                                child: Text("Anteriores",
                                    style: TextStyle(fontSize: 18))),
                          ],
                        ))),
                title: Text('Pedidos'),
              ),
              body: TabBarView(
                children: [
                  ordersProgress.length > 0
                      ? _buildList(ordersProgress)
                      : Center(child: Text("Não há pedido em andamento")),
                  ordersPrevious.length > 0
                      ? _buildList(ordersPrevious)
                      : Center(child: Text("Não há pedidos anteriores")),
                ],
              ),
            ),
          );
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            _displayDialog(context);
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Text("Novo pedido", style: TextStyle(fontSize: 18))
                ],
              )),
          elevation: 5,
        ));
  }

  Widget _buildList(List<Order> list) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(),
      padding: EdgeInsets.all(10.0),
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
            child: Padding(
                padding: EdgeInsets.all(8), child: _buildRow(list[index])),
            onTap: () => {
                  Navigator.pushNamed(context, 'details',
                      arguments: {'order': list[index], 'isHistorical': true})
                });
      },
    );
  }

  _buildRow(Order order) {
    return Column(children: <Widget>[
      Row(children: <Widget>[
        Text("Pedido nº" + order.id.toString(),
            textAlign: TextAlign.start,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Spacer(),
        Text(DateFormat("dd/MM/yyyy HH:mm:ss").format(order.createdAt),
            style: TextStyle(fontSize: 18))
      ]),
      Row(children: <Widget>[
        Expanded(child: Text(order.statusDescription(), style: TextStyle(fontSize: 18)))
      ]),
      Row(children: <Widget>[
        Text(
          NumberFormat.currency(locale: "pt-BR", symbol: "R\$")
              .format(order.value),
          style: TextStyle(fontSize: 18))
      ])
    ]);
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 1; i < 99; i++) {
      items.add(new DropdownMenuItem(
          value: i.toString(), child: new Text(i.toString())));
    }
    return items;
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(child: Text('Novo pedido')),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Selecione a mesa",
                    textAlign: TextAlign.left,
                  ),
                  DropdownButton<String>(
                      value: _table,
                      items: _dropDownMenuItems,
                      onChanged: (String value) {
                        setState(() {
                          _table = value;
                        });
                      },
                      isExpanded: true),
                  Text("Selecione o nº de pessoas", textAlign: TextAlign.left),
                  DropdownButton<String>(
                      value: _amountPeople,
                      items: _dropDownMenuItems,
                      onChanged: (String value) {
                        setState(() {
                          _amountPeople = value;
                        });
                      },
                      isExpanded: true)
                ],
              );
            }),
            actions: <Widget>[
              new RaisedButton(
                child: new Text('CANCELAR'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _amountPeople = null;
                  _table = null;
                },
              ),
              new RaisedButton(
                child: new Text('CONFIRMAR'),
                onPressed: () {
                  if (_amountPeople != null && _table != null) {
                    Order order = new Order(
                        value: 0,
                        //table: int.parse(_table),
                        //amountPeople: int.parse(_amountPeople),
                        status: 0,
                        uiid: Uuid().v4(),
                        createdAt: DateTime.now(),
                        items: []);
                    Navigator.pushNamed(context, 'product',
                        arguments: {'order': order});
                  }
                },
              )
            ],
          );
        });
  }

  void setupList() async {
    isLoading = true;

    Order.fetchOrder().then((value) => {
          if (mounted)
            {
              setState(() {
                ordersPrevious = value["previous"];
                ordersProgress = value["progress"];
                isLoading = false;
              })
            }
        });
  }

  void sendPrint() {}
}
