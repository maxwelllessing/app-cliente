import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListProductVarianceScreen extends StatefulWidget {
  String title;
  ListProductVarianceScreen({this.title});
  @override
  _ListProductVarianceScreenState createState() => new _ListProductVarianceScreenState();
}

class _ListProductVarianceScreenState extends State<ListProductVarianceScreen> {
  int _itemCount = 0;
  @override
  Widget build(BuildContext context) {
    return new ListTile(
      title: new Text(widget.title),
      trailing: new Row(
        children: <Widget>[
          _itemCount!=0? new  IconButton(icon: new Icon(Icons.remove),onPressed: ()=>setState(()=>_itemCount--),):new Container(),
          new Text(_itemCount.toString()),
          new IconButton(icon: new Icon(Icons.add),onPressed: ()=>setState(()=>_itemCount++))
        ],
      ),
    );
  }
}