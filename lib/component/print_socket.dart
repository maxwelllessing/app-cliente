import 'dart:io';

import 'package:fluttersakaesapp/models/item.dart';
import 'package:fluttersakaesapp/models/order.dart';
import 'package:fluttersakaesapp/util.dart';

class PrintSocket {
  static const s = " ";

  print(String print, String ip) async {
    Socket socket = await Socket.connect(ip, 8888);
    socket.write(print);
    socket.close();
  }

  printComanda(Order order, List<Item> items) {
    List<int> printerGroup = [];

    for (int i = 0; i < items.length; i++) {
      if (items[i].printerId != null) {
        bool add = true;
        for(int p = 0; p < printerGroup.length; p++) {
            if(printerGroup[p] == items[i].printerId){
              add =false;
              break;
            }
        }
        if(add)
          printerGroup.add(items[i].printerId);
      }
    }

    String stringToPrint;

    Item item;

    int hasItems;

    String ip;

    for (int p = 0; p < printerGroup.length; p++) {
      stringToPrint = "";
      stringToPrint += paddingRight("COMANDA", 24);
      stringToPrint += paddingLeft(Util.dateFormater(order.createdAt), 24);
      stringToPrint += "\n";
      stringToPrint += paddingRight("MESA " + order.table.toString(), 48);
      stringToPrint += "\n";
      stringToPrint += paddingRight("ITEM", 48);
      stringToPrint += "\n";
      stringToPrint += paddingLeft("QTDE", 48);
      hasItems = 0;
      for (int i = 0; i < items.length; i++) {
        item = items[i];
        if (printerGroup[p] == item.printerId) {
          hasItems++;
          ip = item.ip;
          stringToPrint += "\n";
          if (item.variances != null) {
            stringToPrint += paddingRight(item.name + " " + item.variances, 48);
          }else{
            stringToPrint += paddingRight(item.name, 48);
            if (item.items != null) {
              for (int c = 0; c < item.items.length; c++) {
                stringToPrint += "\n";
                stringToPrint += paddingRight(item.items[c].name + " " + item.items[c].variances, 48);
              }
            }
          }
          stringToPrint += "\n";
          stringToPrint += paddingLeft(" " + item.amount.toString(), 48);
        }
      }
      stringToPrint += "\n";
      if (hasItems > 0) print(Util.removerAcentos(stringToPrint), ip);
    }
  }

  printPedido(Order order, List<Item> items) {
    List<int> printerGroup = [];

    for (int i = 0; i < items.length; i++) {
      if (items[i].printerId != null) {
        bool add = true;
        for(int p = 0; p < printerGroup.length; p++) {
            if(printerGroup[p] == items[i].printerId){
              add =false;
              break;
            }
        }
        if(add)
          printerGroup.add(items[i].printerId);
      }
    }

    String stringToPrint;

    Item item;

    int hasItems;

    String ip;

    for (int p = 0; p < printerGroup.length; p++) {
      stringToPrint = "";
      stringToPrint += paddingRight("PEDIDO " + order.id.toString(), 24);
      stringToPrint += paddingLeft(Util.dateFormater(order.createdAt), 24);
      stringToPrint += "\n";
      stringToPrint += paddingRight("MESA " + order.table.toString(), 48);
      stringToPrint += "\n";
      stringToPrint += paddingRight("ITEM", 48);
      stringToPrint += "\n";
      stringToPrint += paddingRight("VALOR", 24);
      stringToPrint += paddingLeft("QTDE", 24);
      hasItems = 0;
      for (int i = 0; i < items.length; i++) {
        item = items[i];
        if (printerGroup[p] == item.printerId) {
          hasItems++;
          ip = item.ip;
          if (item.variances != null) {
            stringToPrint += paddingRight(item.name + " " + item.variances, 48);
          }else{
            stringToPrint += paddingRight(item.name, 48);
            if (item.items != null) {
              for (int c = 0; c < item.items.length; c++) {
                stringToPrint += "\n";
                stringToPrint += paddingRight(item.items[c].name + " " + item.items[c].variances, 48);
              }
            }
          }
          stringToPrint += "\n";
          stringToPrint +=
              paddingRight("R\$" + Util.numberFormatSymbol(item.value, ""), 24);
          stringToPrint += paddingLeft(" " + item.amount.toString(), 24);
        }
      }
      stringToPrint += "\n";
      stringToPrint += paddingRight(
          "VALOR TOTAL", 24);
          stringToPrint += paddingLeft(
          "R\$" + Util.numberFormatSymbol(order.value, ""), 24);
      stringToPrint += "\n";
      if (hasItems > 0) print(Util.removerAcentos(stringToPrint), ip);
    }
  }

  String paddingRight(String _s, int length) {
    _s = _s.padRight(length, s);
    return _s.toUpperCase();
  }

  String paddingLeft(String _s, int length) {
    _s = _s.padLeft(length, s);
    return _s.toUpperCase();
  }

  String paddingRightChar(String _s, int length, String char) {
    _s = _s.padRight(length, char).toUpperCase();
    return _s;
  }

  String paddingLeftChar(String _s, int length, String char) {
    _s = _s.padLeft(length, char).toUpperCase();
    return _s;
  }
}
