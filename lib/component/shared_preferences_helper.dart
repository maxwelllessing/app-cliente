import 'package:fluttersakaesapp/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static SharedPreferencesHelper sharedPreferencesHelper;

  static SharedPreferences sharedPreferences;

  static Future<SharedPreferencesHelper> instance() async {
    if (sharedPreferencesHelper == null) {
      sharedPreferencesHelper = SharedPreferencesHelper();
      sharedPreferences = await SharedPreferences.getInstance();
    }
    return sharedPreferencesHelper;
  }

  void setToken(String value) {
    sharedPreferences.setString(Constant.API_TOKEN, value);
  }

  String getToken() {
    return sharedPreferences.getString(Constant.API_TOKEN);
  }

  void setUserId(String value) {
    sharedPreferences.setString(Constant.USER_ID, value);
  }

  String getUserId() {
    return sharedPreferences.getString(Constant.USER_ID);
  }
}
