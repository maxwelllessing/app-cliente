import 'package:intl/intl.dart';

class Util {
  static String numberFormat(double _value) {
    return NumberFormat.currency(symbol: "").format(_value).trim();
  }

  static String numberFormatText(double _value) {
    return NumberFormat.currency(locale: "pt-BR", symbol: "R\$")
        .format(_value)
        .trim();
  }

  static String numberFormatSymbol(double _value, String symbol) {
    return NumberFormat.currency(locale: "pt-BR", symbol: symbol)
        .format(_value)
        .trim();
  }


  static String removerAcentos(String texto)
  {
    String comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
    String semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

    for (int i = 0; i < comAcentos.length; i++)
    {
      texto = texto.replaceAll(comAcentos[i], semAcentos[i]);
    }
    return texto;
  }

  static String dateFormater(DateTime data){
    return DateFormat("dd/MM/yyyy HH:mm:ss").format(data);
  }
}
