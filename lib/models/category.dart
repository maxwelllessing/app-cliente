import 'dart:convert';

import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:fluttersakaesapp/models/product.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';

class Category {
  int id;
  String name;
  DateTime createdAt;
  double heightItem = 0;
  double widthItem = 0;
  bool selected = false;

  List<Product> products = [];

  Category(
      {int id,
      double value,
      String name,
      String image,
      String description,
      DateTime createdAt,
      List<Product> products}) {
    this.id = id;
    this.name = name;
    this.createdAt = createdAt;
    this.products = products;
  }

  factory Category.fromMap(Map<String, dynamic> json) {
    return Category(
        id: json['id'],
        name: json['name'],
        createdAt: DateTime.parse(json['created_at']),
        products: Product.parseList(maps: json['products']));
  }

  toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "created_at": this.createdAt,
    };
  }

  static Category parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Category.fromMap(parsed);
  }

  static List<Category> parseList(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return parsed.map<Category>((json) => Category.fromMap(json)).toList();
  }

  static Future<List<Category>> fetch() async {

    String apiToken;

    await SharedPreferencesHelper.instance().then((value) => apiToken = value.getToken());

    final response =
        await http.get(Constant.URL + 'api/products?api_token=' + apiToken, headers: {"Content-Type": "application/json", "token": apiToken});
    if (response.statusCode == 200) {
      return parseList(response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }
}
