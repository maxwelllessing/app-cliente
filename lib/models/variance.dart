import 'dart:convert';

import 'package:http/http.dart' as http;
import '../constant.dart';

class Variance {
  int id;
  double value;
  String name;
  DateTime createdAt;
  double amount;
  int index;

  Variance({int id, double value, String name, DateTime createdAt}) {
    this.id = id;
    this.value = value;
    this.name = name;
    this.createdAt = createdAt;
  }

  factory Variance.fromMap(Map<String, dynamic> json) {
    return Variance(
      id: json['id'],
      value: double.parse(json['value']),
      name: json['name'],
      createdAt: DateTime.parse(json['created_at']),
    );
  }

  toJson() {
    return {
      "id": this.id,
      "value": this.value,
      "name": this.name,
      "created_at": this.createdAt,
    };
  }

  static Variance parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Variance.fromMap(parsed);
  }

  static List<Variance> parseList({String json, List<dynamic> maps}) {
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed.map<Variance>((json) => Variance.fromMap(json)).toList();
    } else {
      return maps.map<Variance>((json) => Variance.fromMap(json)).toList();
    }
  }

  static Future<List<Variance>> fetch(String apiToken) async {
    final response =
        await http.get(Constant.URL + 'api/products?api_token=' + apiToken);
    if (response.statusCode == 200) {
      return parseList(json: response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }
}
