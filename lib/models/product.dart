import 'dart:convert';

import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import 'variance.dart';

class Product {
  int id;
  double value;
  String name;
  String image;
  String description;
  DateTime createdAt;
  int amount = 1;
  List<Variance> variations;
  List<Product> products;
  int printerId;
  String ip;
  int type;

  Product(
      {int id,
      double value,
      String name,
      String image,
      String description,
      DateTime createdAt,
      List<Variance> variations,
      List<Product> products,
      int printerId,
      String ip,
      int type}) {
    this.id = id;
    this.value = value;
    this.name = name;
    this.image = image;
    this.description = description;
    this.createdAt = createdAt;
    this.variations = variations;
    this.products = products;
    this.printerId = printerId;
    this.ip = ip;
    this.type = type;
  }

  factory Product.fromMap(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      value: double.parse(json['value']),
      name: json['name'],
      image: json['image'],
      description: json['description'],
      createdAt: DateTime.parse(json['created_at']),
      variations: Variance.parseList(maps: json['variations']),
      products: Product.parseList(
          maps: json['combos_products'] != null ? json['combos_products'] : []),
      printerId: json['printer_id'],
      ip: json['ip'],
      type: json['type'],
    );
  }

  toJson() {
    List<Map> variations = this.variations != null
        ? this.variations.map((i) => i.toJson()).toList()
        : null;

    List<Map> products = this.products != null
        ? this.products.map((i) => i.toJson()).toList()
        : null;

    return {
      "id": this.id,
      "value": this.value,
      "name": this.name,
      "image": this.image,
      "description": this.description,
      "created_at": this.createdAt,
      "variations": variations,
      "combos_products": products,
      "printer_id": this.printerId,
      "ip": this.ip,
      "type": this.type,
    };
  }

  static Product parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Product.fromMap(parsed);
  }

  static List<Product> parseList({String json, List<dynamic> maps}) {
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed.map<Product>((json) => Product.fromMap(json)).toList();
    } else {
      return maps.map<Product>((json) => Product.fromMap(json)).toList();
    }
  }

  static Future<List<Product>> fetch() async {
    String apiToken;

    await SharedPreferencesHelper.instance()
        .then((value) => apiToken = value.getToken());

    final response = await http.get(
        Constant.URL + 'api/products?api_token=' + apiToken,
        headers: {"Content-Type": "application/json", "token": apiToken});
    if (response.statusCode == 200) {
      return parseList(json: response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }
}
