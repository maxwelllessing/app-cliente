import 'dart:convert';
import 'package:http/http.dart' as http;
import '../constant.dart';

class User {
  int id;
  String email;
  String password;
  String apiToken;
  String name;
  int type;
  String facebookId;
  String googleId;

  User(
      {int id,
      String email,
      String password,
      String apiToken,
      String name,
      int type,
      String facebookId,
      String googleId}) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.apiToken = apiToken;
    this.name = name;
    this.type = type;
    this.facebookId = facebookId;
    this.googleId = googleId;
  }

  factory User.fromMap(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      email: json['email'],
      password: json['password'],
      apiToken: json['api_token'],
      name: json['name'],
      type: json['type'],
      facebookId: json['facebook_id'],
      googleId: json['google_id'],
    );
  }

  toJson() {
    return {
      "email": this.email,
      "password": this.password,
      "api_token": this.apiToken,
      "name": this.name,
      "type": this.type,
      "facebook_id": this.facebookId,
      "google_id": this.googleId
    };
  }

  static User parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return User.fromMap(parsed);
  }

  static Future<User> fetchLogin(User user) async {
    final response = await http.post(Constant.URL + 'api/access',
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(user.toJson()));
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }

  static Future<User> fetchCreate(User user) async {
    final response = await http.post(Constant.URL + 'api/create',
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(user.toJson()));
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }

  static Future<bool> fetchAccess(String apiToken) async {
    final response = await http
        .get(Constant.URL + 'api/access', headers: {"token": apiToken});
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
