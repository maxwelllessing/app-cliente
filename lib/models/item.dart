import 'dart:convert';
import 'package:http/http.dart' as http;

import '../util.dart';

class Item {
  int id;
  double value;
  double amount;
  String name;
  String comments;
  String variances;
  int status;
  int productId;
  int orderId;
  int varianceId;
  String uiid;
  DateTime createdAt;
  int printerId;
  String ip;
  int itemId;

  List<Item> items;

  Item(
      {int id,
      double value,
      double amount,
      String name,
      String comments,
      String variances,
      int status,
      int productId,
      int orderId,
      int varianceId,
      String uiid,
      DateTime createdAt,
      int printerId,
      String ip,
      int itemId,
      List<Item> items}) {
    this.id = id;
    this.value = value;
    this.amount = amount;
    this.name = name;
    this.comments = comments;
    this.variances = variances;
    this.status = status;
    this.productId = productId;
    this.orderId = orderId;
    this.varianceId = varianceId;
    this.uiid = uiid;
    this.createdAt = createdAt;
    this.printerId = printerId;
    this.ip = ip;
    this.itemId = itemId;
    this.items = items;
  }

  factory Item.fromMap(Map<String, dynamic> json) {
    return Item(
      id: json['id'],
      value: double.parse(json['value']),
      amount: double.parse(json['amount']),
      name: json['name'],
      comments: json['comments'],
      variances: json['variances'],
      status: int.parse(json['status'].toString()),
      productId: json['product_id'],
      orderId: json['order_id'],
      varianceId: json['variance_id'],
      uiid: json['uiid'],
      createdAt: DateTime.parse(json['created_at']),
      printerId: json['printer_id'],
      ip: json['ip'],
      itemId: json['item_id'],
      items: Item.parseList(
          maps: json['items'] != null ? json['items'] : []),
    );
  }

  Map toJson() {

    List<Map> items = this.items != null
        ? this.items.map((i) => i.toJson()).toList()
        : null;

    return {
      "id": this.id,
      "value": Util.numberFormat(this.value),
      "amount": Util.numberFormat(this.amount),
      "name": this.name,
      "comments": this.comments,
      "variances": this.variances,
      "status": this.status,
      "product_id": this.productId,
      "order_id": this.orderId,
      "variance_id": this.varianceId,
      "created_at": this.createdAt.toString(),
      "uiid": this.uiid,
      "printer_id": this.printerId,
      "ip": this.ip,
      "item_id": this.itemId,
      "items": items
    };
  }

  static Item parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Item.fromMap(parsed);
  }

  static List<Item> parseList({String json, List<dynamic> maps}) {
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed.map<Item>((json) => Item.fromMap(json)).toList();
    } else {
      return maps.map<Item>((json) => Item.fromMap(json)).toList();
    }
  }
}
