import 'dart:convert';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import '../models/item.dart';
import '../util.dart';

class Order {
  int id;
  double value;
  String table;
  String amountPeople;
  String comments;
  int status;
  int userId;
  DateTime createdAt;
  String uiid;
  int type;
  String name;
  String address;

  List<Item> items = [];

  Order(
      {int id,
      double value,
      String table,
      String amountPeople,
      String comments,
      int status,
      int userId,
      DateTime createdAt,
      String uiid,
      List<Item> items,
      int type,
      String name,
      String address}) {
    this.id = id;
    this.value = value;
    this.table = table;
    this.amountPeople = amountPeople;
    this.comments = comments;
    this.status = status;
    this.userId = userId;
    this.createdAt = createdAt;
    this.uiid = uiid;
    this.items = items;
    this.type = type;
    this.name = name;
    this.address = address;
  }

  factory Order.fromMap(Map<String, dynamic> json) {
    return Order(
        id: json['id'],
        value: double.parse(json['value']),
        table: json['table'],
        amountPeople: json['amount_people'],
        comments: json['comments'],
        status: int.parse(json['status'].toString()),
        userId:
            json['user_id'] != null ? int.parse(json['user_id'].toString()) : 0,
        createdAt: DateTime.parse(json['created_at']),
        uiid: json["uiid"],
        items: Item.parseList(maps: json['items']),
        type: json['type'],
        name: json['name'],
        address: json['address']);
  }

  Map toJson() {
    List<Map> items =
        this.items != null ? this.items.map((i) => i.toJson()).toList() : null;

    return {
      "id": this.id,
      "value": Util.numberFormat(this.value),
      "table": this.table,
      "amount_people": this.amountPeople,
      "comments": this.comments,
      "status": this.status,
      "user_id": this.userId,
      "created_at": this.createdAt.toString(),
      "items": items,
      "uiid": this.uiid,
      "type": this.type,
      "name": this.name,
      "address": this.address,
    };
  }

  static Order parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Order.fromMap(parsed);
  }

  static List<Order> parseList({String json, List<dynamic> maps}) {
    //final parsed = jsonDecode(responseBody);
    //return parsed.map<Order>((json) => Order.fromMap(json)).toList();
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed.map<Order>((json) => Order.fromMap(json)).toList();
    } else {
      return maps.map<Order>((json) => Order.fromMap(json)).toList();
    }
  }

  static Future<List<Order>> fetch() async {
    String apiToken;

    String userId;

    await SharedPreferencesHelper.instance().then(
        (value) => {apiToken = value.getToken(), userId = value.getUserId()});

    final response = await http.get(
      Constant.URL + 'api/orders?user_id=' + userId,
      headers: {"Content-Type": "application/json", "token": apiToken},
    );
    if (response.statusCode == 200) {
      return parseList(json: response.body);
    } else {
      //throw Exception('Unable to fetch products from the REST API');
    }
  }

  static Future<Map<String, dynamic>> fetchOrder() async {
    String apiToken;

    String userId;

    await SharedPreferencesHelper.instance().then(
        (value) => {apiToken = value.getToken(), userId = value.getUserId()});

    final response = await http.get(
      Constant.URL + 'api/orders/historical?user_id=' + userId,
      headers: {"Content-Type": "application/json", "token": apiToken},
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      //return parseList(response.body);
      return {
        "progress": parseList(maps: json["progress"]),
        "previous": parseList(maps: json["previous"])
      };
    } else {
      //throw Exception('Unable to fetch products from the REST API');
    }
  }

  static Future<Order> send(Order order) async {
    String apiToken;

    await SharedPreferencesHelper.instance()
        .then((value) => apiToken = value.getToken());

    var body = jsonEncode(order.toJson());
    final response = await http.post(
        Constant.URL + 'api/order?api_token=' + apiToken,
        headers: {"Content-Type": "application/json", "token": apiToken},
        body: body);
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      return null;
    }
  }

  String statusDescription() {
    if (this.status == 0) {
      return "Seu pedido está esperando confirmação";
    }
    if (this.status == 1) {
      return "Seu pedido está em preparo";
    }
    if (this.status == 2) {
      return "Seu pedido está em rota de entrega";
    }
    if (this.status == 3) {
      return "Seu pedido foi entregue";
    }
    return "Seu pedido foi cancelado pelo restaurante";
  }
}
