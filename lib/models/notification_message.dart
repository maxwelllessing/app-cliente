import 'dart:convert';

import 'package:http/http.dart' as http;
import '../constant.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';

class NotificationMessage {
  int id;
  String title;
  String notification;
  String action;
  DateTime createdAt;
  int userId;

  NotificationMessage(
      {int id,
      String title,
      String notification,
      String action,
      DateTime createdAt,
      int userId}) {
    this.id = id;
    this.title = title;
    this.notification = notification;
    this.action = action;
    this.createdAt = createdAt;
    this.userId = userId;
  }

  factory NotificationMessage.fromMap(Map<String, dynamic> json) {
    return NotificationMessage(
      id: json['id'],
      title: json['title'],
      notification: json['notification'],
      action: json['action'],
      createdAt: DateTime.parse(json['created_at']),
      userId: json['user_id'],
    );
  }

  toJson() {
    return {
      "id": this.id,
      "title": this.title,
      "notification": this.notification,
      "action": this.action,
      "created_at": this.createdAt,
      "user_id": this.userId,
    };
  }

  static NotificationMessage parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return NotificationMessage.fromMap(parsed);
  }

  static List<NotificationMessage> parseList({String json, List<dynamic> maps}) {
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed
          .map<NotificationMessage>((json) => NotificationMessage.fromMap(json))
          .toList();
    } else {
      return maps
          .map<NotificationMessage>((json) => NotificationMessage.fromMap(json))
          .toList();
    }
  }

  static Future<NotificationMessage> fetchNotification() async {
    String apiToken;

    String userId;

    await SharedPreferencesHelper.instance().then(
        (value) => {apiToken = value.getToken(), userId = value.getUserId()});

    final response = await http.get(
      Constant.URL + 'api/notification?user_id=' + userId,
      headers: {"Content-Type": "application/json", "token": apiToken},
    );
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      return null;
    }
  }
}
