import 'package:flutter/material.dart';
import 'screens/add_product_screen.dart';
import 'screens/details_screen.dart';
import 'screens/home_screen.dart';
import 'screens/login_screen.dart';
import 'screens/product_screen.dart';
import 'screens/splash_screen.dart';
import 'screens/order_screen.dart';

final routes = {
  '/': (BuildContext context) => new HomeScreen(),
  'login': (BuildContext context) => new LoginScreen(),
  'splash': (BuildContext context) => new SplashScreen(),
  'product': (BuildContext context) => new ProductScreen(),
  'add_product_dialog': (BuildContext context) => new AddProuctScreenDialog(),
  'details': (BuildContext context) => new DetailsScreen(),
  'orders': (BuildContext context) => new OrderScreen()
};
